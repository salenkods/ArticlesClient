﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleClient.Commands
{
    public class DelegateCommand<T> : DelegateCommandBase
    {
        public DelegateCommand(Action<T> action)
            : base((o) => action((T)o), (o) => true)
        {
        }

        public DelegateCommand(Action<T> action, Func<T, bool> canExecute)
            : base((o) => action((T)o), (o) => canExecute((T)o))
        {
        }
    }

    public class DelegateCommand : DelegateCommandBase
    {
        public DelegateCommand(Action action)
            : base((o) => action(), (o) => true)
        {
        }

        public DelegateCommand(Action action, Func<bool> canExecute)
            : base((o) => action(), (o) => true)
        {
        }
    }
}
