﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticlesClient.ViewModels.Items
{
    public class ArticleItemViewModel : PropertyChangedBase
    {
        public int Id
        {
            get { return _id; }
            set { this.SetProperty(ref _id, value); }
        }

        public string Title
        {
            get { return _title; }
            set { this.SetProperty(ref _title, value); }
        }

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { this.SetProperty(ref _dateCreated, value); }
        }

        public DateTime DateModified
        {
            get { return _dateModified; }
            set { this.SetProperty(ref _dateModified, value); }
        }

        private int _id;
        private string _title;
        private DateTime _dateCreated;
        private DateTime _dateModified;
    }
}
