﻿using ArticlesClient.Services.Online.Articles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticlesClient.Messages
{
    public class EditArticleMessage
    {
        public EditArticleMessage(Article article)
        {
            Article = article;
        }

        public Article Article { get; private set; }
    }
}
