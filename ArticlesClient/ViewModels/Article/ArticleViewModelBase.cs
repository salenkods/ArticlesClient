﻿using ArticleClient.Commands;
using ArticlesClient.Services.Online.Articles;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ArticlesClient.ViewModels.Article
{
    public abstract class ArticleViewModelBase : Screen
    {
        public ArticleViewModelBase(
            IArticleService articleService)
        {
            _articleService = articleService;

            CloseCommand = new DelegateCommand(OnClose);
        }

        public ICommand CloseCommand { get; private set; }

        public string Title
        {
            get { return _title; }
            set
            {
                if (this.SetProperty(ref _title, value))
                {
                    OnTitleChanged();
                }
            }
        }

        public string Text
        {
            get { return _text; }
            set
            {
                if (this.SetProperty(ref _text, value))
                {
                    OnTextChanged();
                }
            }

        }

        public bool IsLoading
        {
            get { return _isLoading; }
            set { this.SetProperty(ref _isLoading, value); }
        }

        protected virtual void OnTitleChanged()
        {
        }

        protected virtual void OnTextChanged()
        {
        }

        private void OnClose()
        {
            TryClose();
        }

        protected readonly IArticleService _articleService;
        private string _title;
        private string _text;
        private bool _isLoading;
    }
}