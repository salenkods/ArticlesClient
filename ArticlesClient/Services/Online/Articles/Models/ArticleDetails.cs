﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ArticlesClient.Services.Online.Articles.Models
{
    [DataContract]
    public class ArticleDetails : Article
    {
        [DataMember]
        public string Text { get; set; }
    }
}
