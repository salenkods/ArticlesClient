﻿using ArticleClient.Commands;
using ArticlesClient.Messages;
using ArticlesClient.Services.Online.Articles;
using ArticlesClient.ViewModels.Article;
using ArticlesClient.ViewModels.Items;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ArticlesClient.ViewModels
{
    public class MainViewModel
        : Screen
        , IHandle<AddArticleMessage>
        , IHandle<EditArticleMessage>
    {
        public MainViewModel(
            IWindowManager windowManager,
            IArticleService articleService,
            IEventAggregator eventAggregator)
        {
            _windowManager = windowManager;
            _articleService = articleService;
            _eventAggregator = eventAggregator;

            AddArticleCommand = new DelegateCommand(OnAddArticle);
            ReadArticleCommand = new DelegateCommand(OnReadArticle);
            EditArticleCommand = new DelegateCommand(OnEditArticle);
            DeleteArticleCommand = new DelegateCommand(OnDeleteArticle);
        }

        public ICommand AddArticleCommand { get; private set; }
        public ICommand ReadArticleCommand { get; private set; }
        public ICommand EditArticleCommand { get; private set; }
        public ICommand DeleteArticleCommand { get; private set; }

        public ObservableCollection<ArticleItemViewModel> Articles
        {
            get { return _articles; }
            set { this.SetProperty(ref _articles, value); }
        }

        public ArticleItemViewModel SelectedArticle
        {
            get { return _selectedArticle; }
            set { this.SetProperty(ref _selectedArticle, value); }
        }

        public bool IsLoading
        {
            get { return _isLoading; }
            set { this.SetProperty(ref _isLoading, value); }
        }

        protected async override void OnActivate()
        {
            base.OnActivate();
            _eventAggregator.Subscribe(this);
            await LoadArticlesAsync();
        }

        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
            _eventAggregator.Unsubscribe(this);
        }

        private async Task LoadArticlesAsync()
        {
            IsLoading = true;

            try
            {
                var articles = await _articleService.GetArticlesAsync();
                Articles = new ObservableCollection<ArticleItemViewModel>(
                    articles.Select(x => new ArticleItemViewModel()
                    {
                        Id = x.Id,
                        Title = x.Title,
                        DateCreated = x.DateCreated,
                        DateModified = x.DateModified
                    }));
            }
            catch (Exception ex)
            {
                // TODO: Show exception to user.
            }

            IsLoading = false;
        }

        private void OnAddArticle()
        {
            _windowManager.ShowDialog(new AddArticleViewModel(_articleService, _eventAggregator));
        }

        private void OnReadArticle()
        {
            _windowManager.ShowDialog(new ReadArticleViewModel(SelectedArticle.Id, _articleService));
        }

        private void OnEditArticle()
        {
            _windowManager.ShowDialog(new EditArticleViewModel(SelectedArticle.Id, _articleService, _eventAggregator));
        }

        private async void OnDeleteArticle()
        {
            IsLoading = true;

            try
            {
                await _articleService.DeleteArticleAsync(SelectedArticle.Id);
                Articles.Remove(SelectedArticle);
            }
            catch
            {
                // TODO: show error
            }

            IsLoading = false;
        }

        public void Handle(AddArticleMessage message)
        {
            Articles.Add(new ArticleItemViewModel()
            {
                Id = message.Article.Id,
                Title = message.Article.Title,
                DateCreated = message.Article.DateCreated,
                DateModified = message.Article.DateModified
            });
        }

        public void Handle(EditArticleMessage message)
        {
            var article = Articles.FirstOrDefault(x => x.Id == message.Article.Id);
            if (article != null)
            {
                article.Title = message.Article.Title;
                article.DateModified = message.Article.DateModified;
            }
        }

        private readonly IWindowManager _windowManager;
        private readonly IArticleService _articleService;
        private readonly IEventAggregator _eventAggregator;
        private ObservableCollection<ArticleItemViewModel> _articles;
        private ArticleItemViewModel _selectedArticle;
        private bool _isLoading;
    }
}