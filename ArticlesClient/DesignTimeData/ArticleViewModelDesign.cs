﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticlesClient.DesignTimeData
{
    public class ArticleViewModelDesign
    {
        public ArticleViewModelDesign()
        {
            Title = "A friend in need is a friend indeed";
            Text = "Once upon a time there lived a lion in a forest. One day after a heavy meal. It was sleeping under a tree. After a while, there came a mouse and it started to play on the lion. Suddenly the lion got up with anger and looked for those who disturbed its nice sleep. Then it saw a small mouse standing trembling with fear. The lion jumped on it and started to kill it. The mouse requested the lion to forgive it. The lion felt pity and left it. The mouse ran away. On another day, the lion was caught in a net by a hunter.The mouse came there and cut the net. Thus it escaped.There after, the mouse and the lion became friends. They lived happily in the forest afterwards.";
            IsLoading = false;
        }

        public string Title { get; set; }
        public string Text { get; set; }
        public bool IsLoading { get; set; }
    }
}
