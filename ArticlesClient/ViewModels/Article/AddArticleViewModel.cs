﻿using ArticleClient.Commands;
using ArticlesClient.Messages;
using ArticlesClient.Services.Online.Articles;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ArticlesClient.ViewModels.Article
{
    public class AddArticleViewModel : ArticleViewModelBase
    {
        public AddArticleViewModel(
            IArticleService articleService,
            IEventAggregator eventAggregator)
            : base(articleService)
        {
            _eventAggregator = eventAggregator;

            AddCommand = new DelegateCommand(OnAdd);
        }

        public ICommand AddCommand { get; private set; }

        public bool IsAddEnabled
        {
            get { return _isAddEnabled; }
            set { this.SetProperty(ref _isAddEnabled, value); }
        }

        protected override void OnTitleChanged()
        {
            UpdateIsAddEnabled();
        }

        protected override void OnTextChanged()
        {
            UpdateIsAddEnabled();
        }

        private void UpdateIsAddEnabled()
        {
            IsAddEnabled = !String.IsNullOrWhiteSpace(Title) && !String.IsNullOrWhiteSpace(Text);
        }

        private async void OnAdd()
        {
            IsLoading = true;

            try
            {
                var article = await _articleService.CreateArticleAsync(
                    new Services.Online.Articles.Models.ArticleDetails()
                    {
                        Text = Text,
                        Title = Title,
                        DateCreated = DateTime.UtcNow,
                        DateModified = DateTime.UtcNow
                    });
                _eventAggregator.PublishOnUIThread(new AddArticleMessage(article));
                TryClose();
            }
            catch
            {
                // TODO: Show error
            }

            IsLoading = false;
        }

        private readonly IEventAggregator _eventAggregator;
        private bool _isAddEnabled;
    }
}
