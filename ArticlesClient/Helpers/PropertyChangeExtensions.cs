﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Caliburn.Micro
{
    public static class PropertyChangeExtensions
    {
        public static bool SetProperty<T>(
            this PropertyChangedBase o,
            ref T storage,
            T value,
            [CallerMemberName] string propertyName = null)
        {
            if (!Equals(storage, value))
            {
                storage = value;
                o.NotifyOfPropertyChange(propertyName);
                return true;
            }

            return false;
        }
    }
}
