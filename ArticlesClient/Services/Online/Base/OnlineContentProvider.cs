﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ArticlesClient.Services.Online.Base
{
    public abstract class OnlineContentProvider
    {
        protected async Task<string> SendAsync(HttpRequestMessage message)
        {
            try
            {
                // Always stop previous request before starting new.
                CancelOperations();

                var result = await _httpClient.SendAsync(message, _cts.Token);

                ThrowExceptionForBadResult(result);

                var str = await result.Content.ReadAsStringAsync();
                return str;
            }
            catch (Exception ex)
            {
                if (NetworkInterface.GetIsNetworkAvailable() == false)
                {
                    throw new NoInternetException();
                }
                else
                {
                    throw ex;
                }
            }
        }

        private void ThrowExceptionForBadResult(HttpResponseMessage message)
        {
            if (!message.IsSuccessStatusCode)
            {
                switch (message.StatusCode)
                {
                    default:
                        throw new Exception(message.ReasonPhrase);
                }
            }
        }

        private void CancelOperations()
        {
            if (_cts != null)
            {
                _cts.Cancel();
                _cts.Dispose();
                _cts = new CancellationTokenSource();
            }
        }

        private HttpClient _httpClient = new HttpClient();
        private CancellationTokenSource _cts = new CancellationTokenSource();
    }
}
