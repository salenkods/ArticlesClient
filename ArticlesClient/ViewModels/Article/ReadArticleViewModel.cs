﻿using ArticleClient.Commands;
using ArticlesClient.Services.Online.Articles;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ArticlesClient.ViewModels.Article
{
    public class ReadArticleViewModel : ArticleViewModelBase
    {
        public ReadArticleViewModel(
            int articleId,
            IArticleService articleService)
            : base(articleService)
        {
            _articleId = articleId;
        }

        protected async override void OnActivate()
        {
            base.OnActivate();

            IsLoading = true;

            try
            {
                var article = await _articleService.GetArticleAsync(_articleId);

                Title = article.Title;
                Text = article.Text;
            }
            catch (Exception ex)
            {
                // Show error to user.
            }

            IsLoading = false;
        }

        private readonly int _articleId;
    }
}
