﻿using ArticlesClient.Services.Online.Articles;
using ArticlesClient.ViewModels;
using ArticlesClient.ViewModels.Article;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ArticlesClient
{
    public class ShellBootstrapper : BootstrapperBase
    {
        public ShellBootstrapper() : base(true)
        {
            Initialize();
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            return _container.GetInstance(serviceType, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _container.GetAllInstances(serviceType);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void Configure()
        {
            base.Configure();

            _container = new SimpleContainer();

            RegisterServices();
            RegisterViewModels();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            base.OnStartup(sender, e);
            DisplayRootViewFor<MainViewModel>();
        }

        private void RegisterServices()
        {
            _container.Singleton<IWindowManager, WindowManager>();
            _container.Singleton<IEventAggregator, EventAggregator>();
            _container.Singleton<IArticleService, ArticleService>();
        }

        private void RegisterViewModels()
        {
            _container.PerRequest<MainViewModel>();
            _container.PerRequest<ReadArticleViewModel>();
            _container.PerRequest<AddArticleViewModel>();
            _container.PerRequest<EditArticleViewModel>();
        }

        private SimpleContainer _container;
    }
}
