﻿using ArticleClient.Commands;
using ArticlesClient.Messages;
using ArticlesClient.Services.Online.Articles;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ArticlesClient.ViewModels.Article
{
    public class EditArticleViewModel : ArticleViewModelBase
    {
        public EditArticleViewModel(
            int articleId,
            IArticleService articleService,
            IEventAggregator eventAggregator)
            : base(articleService)
        {
            _articleId = articleId;
            _eventAggregator = eventAggregator;

            SaveCommand = new DelegateCommand(OnSave);
        }

        public ICommand SaveCommand { get; private set; }

        public bool IsSaveEnabled
        {
            get { return _isSaveEnabled; }
            set { this.SetProperty(ref _isSaveEnabled, value); }
        }

        protected async override void OnActivate()
        {
            base.OnActivate();

            IsLoading = true;

            try
            {
                var article = await _articleService.GetArticleAsync(_articleId);

                Title = article.Title;
                Text = article.Text;
            }
            catch (Exception ex)
            {
                // Show error to user.
            }

            IsLoading = false;
        }

        protected override void OnTitleChanged()
        {
            UpdateIsAddEnabled();
        }

        protected override void OnTextChanged()
        {
            UpdateIsAddEnabled();
        }

        private void UpdateIsAddEnabled()
        {
            IsSaveEnabled = !String.IsNullOrWhiteSpace(Title) && !String.IsNullOrWhiteSpace(Text);
        }

        private async void OnSave()
        {
            IsLoading = true;

            try
            {
                await _articleService.EditArticleAsync(_articleId, new Services.Online.Articles.Models.ArticleDetails()
                {
                    Title = Title,
                    Text = Text,
                    DateModified = DateTime.Now,
                });

                _eventAggregator.PublishOnUIThread(new EditArticleMessage(new Services.Online.Articles.Models.Article()
                {
                    Id = _articleId,
                    Title = Title,
                    DateModified = DateTime.UtcNow
                }));

                TryClose();
            }
            catch
            {
                // TODO: show error
            }

            IsLoading = false;
        }

        private readonly int _articleId;
        private readonly IEventAggregator _eventAggregator;
        private bool _isSaveEnabled;
    }
}