﻿using ArticlesClient.Services.Online.Articles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticlesClient.Services.Online.Articles
{
    public interface IArticleService
    {
        Task<List<Article>> GetArticlesAsync();
        Task<ArticleDetails> GetArticleAsync(int articleId);
        Task<Article> CreateArticleAsync(ArticleDetails article);
        Task EditArticleAsync(int id, ArticleDetails article);
        Task DeleteArticleAsync(int id);
    }
}
