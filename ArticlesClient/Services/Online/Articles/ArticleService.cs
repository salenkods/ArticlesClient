﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArticlesClient.Services.Online.Articles.Models;
using System.Net.Http;
using ArticlesClient.Services.Online.Base;
using Newtonsoft.Json;

namespace ArticlesClient.Services.Online.Articles
{
    public class ArticleService : OnlineContentProvider, IArticleService
    {
        public async Task<List<Article>> GetArticlesAsync()
        {
            string uri = $"{BASE_URL}{"/api/articles"}";

            var message = new HttpRequestMessage(HttpMethod.Get, new Uri(uri));

            string json = await SendAsync(message);

            var articles = JsonConvert.DeserializeObject<List<Article>>(json);
            return articles;
        }

        public async Task<ArticleDetails> GetArticleAsync(int articleId)
        {
            string uri = $"{BASE_URL}{"/api/articles/"}{articleId}";

            var message = new HttpRequestMessage(HttpMethod.Get, new Uri(uri));

            string json = await SendAsync(message);

            var article = JsonConvert.DeserializeObject<ArticleDetails>(json);
            return article;
        }

        public async Task<Article> CreateArticleAsync(ArticleDetails article)
        {
            string uri = $"{BASE_URL}{"/api/articles"}";

            var message = new HttpRequestMessage(HttpMethod.Post, new Uri(uri));
            message.Content = new StringContent(JsonConvert.SerializeObject(article), Encoding.UTF8, "application/json");

            string json = await SendAsync(message);

            var createdArticle = JsonConvert.DeserializeObject<Article>(json);
            return createdArticle;
        }

        public async Task EditArticleAsync(int id, ArticleDetails article)
        {
            string uri = $"{BASE_URL}{"/api/articles/"}{id}";

            var message = new HttpRequestMessage(HttpMethod.Put, new Uri(uri));
            message.Content = new StringContent(JsonConvert.SerializeObject(article), Encoding.UTF8, "application/json");

            string json = await SendAsync(message);
        }

        public async Task DeleteArticleAsync(int id)
        {
            string uri = $"{BASE_URL}{"/api/articles/"}{id}";

            var message = new HttpRequestMessage(HttpMethod.Delete, new Uri(uri));

            string json = await SendAsync(message);
        }

        private const string BASE_URL = "http://localhost:64359";
    }
}
