﻿using ArticlesClient.ViewModels.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticlesClient.DesignTimeData
{
    public class MainViewModelDesign
    {
        public MainViewModelDesign()
        {
            Articles = new List<ArticleItemViewModel>()
            {
                new ArticleItemViewModel()
                {
                    Id = 1,
                    Title = "A friend in need is a friend indeed",
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow,
                },
                new ArticleItemViewModel()
                {
                    Id = 2,
                    Title = "A Town Mouse and A Country Mouse",
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow,
                },
            };

            IsLoading = false;
        }

        public List<ArticleItemViewModel> Articles { get; set; }
        public bool IsLoading { get; set; }
    }
}
